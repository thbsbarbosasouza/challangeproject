/**
 * @description       : The interface will test whether the text is nested correctly. 
 * @author            : Thiago Barbosa
 * @group             : 
 * @last modified on  : 07-26-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
public with sharing class WidgetController {
    
    private static final String WIDGET_CREATED = 'Widget Created'; 
    private static final String ENDPOINT_WIDGET = 'https://a81fb8de4d0a38b857afb8fb347e57c3.m.pipedream.net'; 


  
   
    /**
    * @description 
    * @author THBS | 07-26-2021 
    * @param widgetRecord 
    * @param aRecordId 
    * @param value 
    * @return Module_Tracke15__Widget__c 
    **/
    @AuraEnabled
    public static Module_Tracke15__Widget__c getCreatedWidgets(Module_Tracke15__Widget__c widgetRecord, String aRecordId, String value) {
        
        Module_Tracke15__Widget__c widget = new Module_Tracke15__Widget__c();  

        try{
        System.debug('aRecordId' + aRecordId); 
        System.debug('antes do if'); 
        if(String.isNotBlank(aRecordId)){  
            
            widget.Module_Tracke15__Account__c = aRecordId;    
            widget.Name = Label.customWidget.replace('{0}' , String.valueOf(widgetRecord.Module_Tracke15__WidgetSession__c));
            widget.Module_Tracke15__Value__c = value;  
            widget.Module_Tracke15__Is_Properly_Nested__c = value.contains('') ? true : false;
            System.debug('aRecordId'); 
        } 
        if(widget.Module_Tracke15__Is_Properly_Nested__c == false){
            System.debug('if False'); 
            return widget; 
        }  
        else if(widget.Module_Tracke15__Is_Properly_Nested__c == true){
            System.debug('widget.Module_Tracke15__Is_Properly_Nested__c' + widget.Module_Tracke15__Is_Properly_Nested__c);  
            Database.insert(widget);   

            getCalloutWidget(JSON.serialize(widget)); 
            
            
        }

        return widget; 
        
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
    }

    /** 
    * @description: get All Widget Test
    * @author Thiago Barbosa | 07-24-2021 
    * @return Module_Tracke15__Widget__c 
    **/
    @AuraEnabled
    public static List<Module_Tracke15__Widget__c> getAllWidgetTest(String aRecordId){

        try {
            
            return [SELECT Id, Module_Tracke15__Account__c, Module_Tracke15__Value__c, Module_Tracke15__Is_Properly_Nested__c
                   FROM Module_Tracke15__Widget__c WHERE Id =:aRecordId]; 

            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

 /**
 * @description get Delete Widget
 * @author THBS | 07-24-2021 
 * @param aRecordId 
 * @return Module_Tracke15__Widget__c 
 **/
 @AuraEnabled
 public static Module_Tracke15__Widget__c getDeleteWidget(String aRecordId){
     try {

        Module_Tracke15__Widget__c widgetToDelete = new Module_Tracke15__Widget__c();
        widgetToDelete.Id = aRecordId;   

        Database.delete(widgetToDelete);

        return widgetToDelete; 
         
     } catch (Exception e) {
         throw new AuraHandledException(e.getMessage());
     } 
    }


 /**
 * @description: Web service or smart phone application which will accept the value from the user 
 * @author THBS | 07-26-2021 
 * @param aWidget  
 **/
 @future(callout=true)
 public static void getCalloutWidget(String aWidget){

            Http http = new Http();
            HttpRequest request = new HttpRequest(); 
            request.setEndpoint(ENDPOINT_WIDGET);
            request.setMethod('POST');  
            request.setHeader('Content-Type', 'application/json;charset=UTF-8');
            System.debug('aWidget' + aWidget);   
            request.setBody(JSON.serialize(aWidget)); 
            HttpResponse response = http.send(request); 
            if (response.getStatusCode() == 200) {
            // Deserializes the JSON string into collections of primitive data types.
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                
            } 
    }

}
