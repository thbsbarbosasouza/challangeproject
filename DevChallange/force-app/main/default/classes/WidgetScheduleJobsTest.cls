/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-26-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
@isTest
public with sharing class WidgetScheduleJobsTest {
    


    @isTest static void testSchedule(){

        Account account = new Account();
        account.Name = 'Test Account';
        insert account; 

        Module_Tracke15__Widget__c widget = new Module_Tracke15__Widget__c();
        widget.Module_Tracke15__Account__c = account.Id;    
        widget.Name = 'Test Name 2';
        widget.OwnerId = UserInfo.getUserId(); 
        widget.Module_Tracke15__WidgetSession__c = '1';  
        widget.Module_Tracke15__Value__c = 'Teste Account';  
        widget.Module_Tracke15__Is_Properly_Nested__c = true; 

        Database.insert(widget); 
        Database.BatchableContext BC;  

        Test.startTest();
            WidgetScheduleJobs lBatchWidget = new WidgetScheduleJobs(); 
            lBatchWidget.execute(BC, new List<Module_Tracke15__Widget__c>{widget});

        Test.stopTest(); 
        System.assert(lBatchWidget!= null); 

    }



    @isTest static void testScheduleFalse(){

        Account account = new Account(); 
        account.Name = 'Test Account';
        insert account; 

        Module_Tracke15__Widget__c widget = new Module_Tracke15__Widget__c();
        widget.Module_Tracke15__Account__c = account.Id;    
        widget.Name = 'Test Name 2';
        widget.OwnerId = UserInfo.getUserId();  
        widget.Module_Tracke15__WidgetSession__c = '1';  
        widget.Module_Tracke15__Value__c = 'Teste Account';  
        widget.Module_Tracke15__Is_Properly_Nested__c = false; 

        Database.insert(widget);  
        Database.BatchableContext BC;  
        SchedulableContext sc;

        Test.startTest();
            WidgetScheduleJobs lBatchWidget = new WidgetScheduleJobs(); 
            lBatchWidget.execute(BC, new List<Module_Tracke15__Widget__c>{widget});
            lBatchWidget.finish(BC);     
            lBatchWidget.scheduleBatchStatus();

        Test.stopTest(); 
        System.assert(lBatchWidget!= null, 'The Widget was inserted'); 

    }
}
