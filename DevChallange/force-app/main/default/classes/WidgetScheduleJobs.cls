/**
 * @description       : chanllange Enpal
 * @author            : Thiago Barbosa
 * @group             : 
 * @last modified on  : 07-26-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
global with sharing class WidgetScheduleJobs implements Database.batchable<sObject>, Schedulable{

    private static final String WIDGET_CREATED = 'Widget Created'; 
    global Database.QueryLocator start(Database.BatchableContext bc){
        String query = 'SELECT id, Module_Tracke15__Is_Properly_Nested__c, Module_Tracke15__Value__c from 	Module_Tracke15__Widget__c WHERE Id != null';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Module_Tracke15__Widget__c> scope)
    {
        List<Module_Tracke15__Widget__c> newWidgetList = new  List<Module_Tracke15__Widget__c>();

        
         for(Module_Tracke15__Widget__c iWidget : scope ) 
     { 
        
            Module_Tracke15__Widget__c widget = new Module_Tracke15__Widget__c();
            widget.Module_Tracke15__Is_Properly_Nested__c = iWidget.Module_Tracke15__Is_Properly_Nested__c ? true : false;
            widget.Module_Tracke15__Value__c = WIDGET_CREATED;
            widget.OwnerId = UserInfo.getUserId(); 
            newWidgetList.add(widget); 
        
    }

    if(!newWidgetList.isEmpty()){  
        Database.insert(newWidgetList);
        List<Module_Tracke15__Widget__c> updateWidgetList = new List<Module_Tracke15__Widget__c>();
        List<Messaging.SingleEmailMessage> mailsList = new List<Messaging.SingleEmailMessage>(); 
        for(Module_Tracke15__Widget__c iWidget : newWidgetList){
            if(iWidget.Module_Tracke15__Is_Properly_Nested__c == true){
                iWidget.Module_Tracke15__Value__c = 'Is properly nested';
                iWidget.OwnerId = UserInfo.getUserId();
                updateWidgetList.add(iWidget);   
            }
            else if(iWidget.Module_Tracke15__Is_Properly_Nested__c == false){
                
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.setSubject(iWidget.Name); 
                message.setPlainTextBody('Email Sended');
                message.setSenderDisplayName(iWidget.Owner.Name);
                message.setReplyTo(iWidget.Owner.Email);   
                string[] toaddress = New String[] {'thiagotbs19@hotmail.com'};
                message.setToAddresses(toaddress);  
                message.setBccSender(false);
                message.setUseSignature(false);
                message.setPlainTextBody('The value is not properly nested corresponding to id ' + iWidget.Id);
                
                message.setHtmlBody('The value is not properly nested corresponding to id' + iWidget.Id + 
                    'To view your Widget <a href=https://yourInstance.salesforce.com/'+iWidget.Id+'>click here.</a>'); 
                
                mailsList.add(message);
                
                                
            }
            Messaging.sendEmail(mailsList);  
        }
        if(!updateWidgetList.isEmpty()){ 
            Database.update(updateWidgetList);
        }
    }
    

}

global void execute(SchedulableContext sc){
    Database.executebatch(new WidgetScheduleJobs(), 100);
}

   
 global void finish(Database.BatchableContext BC)
    {
    }

    global void scheduleBatchStatus(){
		String schedulingTime = '0 0 15 * * ?'; //every day at 3 pm
		WidgetScheduleJobs loadScheduleJobs = new WidgetScheduleJobs();
		String jobName = 'WidgetScheduleJobs' + System.now().format('yyyyMMdd_HHmmssSSS');
		System.schedule(jobName, schedulingTime, loadScheduleJobs); 
	} 
}