/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-26-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
@isTest
public with sharing class WidgetControllerTest {
    

    private static final String SYSTEM_ADM = 'System Administrator';
     private static String jsonRequest;
    
    private class RestMock200 implements HttpCalloutMock {
      	 

        public HTTPResponse respond(HTTPRequest req) {
            
            

            HTTPResponse res = new HTTPResponse();
            res.setHeader('Authorization', 'key');
            res.setHeader('Accept','text/json');
            res.setHeader('Content-Type', 'text/json');
            res.setBody(jsonRequest);
            res.setStatusCode(200);
            return res;
        }
    }

    private class RestMock400 implements HttpCalloutMock {
      	 

        public HTTPResponse respond(HTTPRequest req) {   
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'text/json');
            res.setHeader('Authorization', 'key');
            res.setHeader('Accept','text/json');
            res.setBody(jsonRequest);
            res.setStatusCode(400);
            return res;
        }
    }

    /**
    * @description 
    * @author THBS | 07-26-2021 
    **/
    @isTest static void testgetCreatedWidgets(){

        Account account = new Account();
        account.Name = 'Test Account';
        insert account; 

        Module_Tracke15__Widget__c widget = new Module_Tracke15__Widget__c();
        widget.Module_Tracke15__Account__c = account.Id;    
        widget.Name = 'Test Name 2';
        widget.Module_Tracke15__WidgetSession__c = '1';  
        widget.Module_Tracke15__Value__c = 'Teste Account';  
        widget.Module_Tracke15__Is_Properly_Nested__c = true; 

        Database.insert(widget); 
        jsonRequest = JSON.serialize(widget);
        Test.setMock(HttpCalloutMock.class, new RestMock200());   
        Test.startTest();    
            widget = WidgetController.getCreatedWidgets(widget, account.Id, 'Teste Value');
        Test.stopTest();   
        System.assert(widget!= null, 'The Widget Was inserted');
    }


    /**
    * @description 
    * @author THBS | 07-26-2021 
    **/
    @isTest static void testgetDeleteWidget(){

        Account account = new Account();
        account.Name = 'Test Account';
        insert account; 

        Module_Tracke15__Widget__c widget = new Module_Tracke15__Widget__c();
        widget.Module_Tracke15__Account__c = account.Id;    
        widget.Name = 'Test Name 2';
        widget.Module_Tracke15__WidgetSession__c = '1';  
        widget.Module_Tracke15__Value__c = 'Teste Account';  
        widget.Module_Tracke15__Is_Properly_Nested__c = true; 

        insert widget;

        Test.startTest();
            widget = WidgetController.getDeleteWidget(widget.Id);
        Test.stopTest(); 
        System.assert(widget != null, 'The widget was deleted'); 
    }


    @isTest static void testgetAllWidgetTestException(){

        Account account = new Account();
        account.Name = 'Test Account';
        insert account; 

        Module_Tracke15__Widget__c widget = new Module_Tracke15__Widget__c();
        widget.Module_Tracke15__Account__c = account.Id;    
        widget.Name = 'Test Name 2';
        widget.Module_Tracke15__WidgetSession__c = '1';  
        widget.Module_Tracke15__Value__c = 'Teste Account';  
        widget.Module_Tracke15__Is_Properly_Nested__c = true; 

        insert widget;
        try{ 
            Test.startTest();  
            List<Module_Tracke15__Widget__c> widgetlist =  WidgetController.getAllWidgetTest('');
            Test.stopTest();  
            System.assert(widgetlist.isEmpty(), 'The method return Exception ');
        }catch(Exception ex){  
            System.debug(ex.getMessage() + 'Return Message Error');
        } 
    } 

    @isTest static void testgetAllWidgetTest(){

        Account account = new Account();
        account.Name = 'Test Account';
        insert account; 

        Module_Tracke15__Widget__c widget = new Module_Tracke15__Widget__c();
        widget.Module_Tracke15__Account__c = account.Id;    
        widget.Name = 'Test Name 2';
        widget.Module_Tracke15__WidgetSession__c = '1';  
        widget.Module_Tracke15__Value__c = 'Teste Account';  
        widget.Module_Tracke15__Is_Properly_Nested__c = true; 

        insert widget;

        Test.startTest();
        List<Module_Tracke15__Widget__c> widgetList = WidgetController.getAllWidgetTest(widget.Id); 
        Test.stopTest();  
        System.assert(widget != null, 'The Widget was return with success');
    }


    @isTest static void testgetAllWidgetTestnotNested(){

        Account account = new Account();
        account.Name = 'Test Account'; 
        insert account; 

        Module_Tracke15__Widget__c widget = new Module_Tracke15__Widget__c();
        widget.Module_Tracke15__Account__c = account.Id;    
        widget.Name = 'Test Name 2';   
        widget.Module_Tracke15__Value__c = null;  
        widget.Module_Tracke15__Is_Properly_Nested__c = true; 

        insert widget;
        try{
            Test.startTest();
                Module_Tracke15__Widget__c widgetResult = WidgetController.getCreatedWidgets(widget, account.Id, 'Teste');
            Test.stopTest();       
            System.assert(widget != null, 'The Widget was return with success');
        }catch(Exception ex){  
            System.debug('ex.getMessage()' + ex.getMessage()); 
        }
    }



    @isTest static void testgetAllWidgetTestnotDelete(){

        Account account = new Account(); 
        account.Name = 'Test Account'; 
        insert account; 

        Module_Tracke15__Widget__c widget = new Module_Tracke15__Widget__c();
        widget.Module_Tracke15__Account__c = account.Id;    
        widget.Name = 'Test Name 2';   
        widget.Module_Tracke15__Value__c = null;  
        widget.Module_Tracke15__Is_Properly_Nested__c = true; 

        insert widget;
        try{
            Test.startTest();
                Module_Tracke15__Widget__c widgetResult = WidgetController.getDeleteWidget(account.Id);
            Test.stopTest();        
            System.assert(widget != null, 'The Widget was return with success');
        }catch(Exception ex){  
            System.debug('ex.getMessage()' + ex.getMessage()); 
        }
    }
}
