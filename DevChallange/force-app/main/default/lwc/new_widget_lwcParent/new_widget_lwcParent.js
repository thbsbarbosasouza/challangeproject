/**
 * @Author Thiago Barbosa
 * @date  2021-07-24 
 * @description Widget Record 
 **/

 import { LightningElement, api, track, wire } from "lwc";
 import getAllWidgetTest from "@salesforce/apex/WidgetController.getAllWidgetTest";
 import { CurrentPageReference, NavigationMixin } from "lightning/navigation";
 import {  
   registerListener,
   unregisterAllListeners, 
   fireEvent
 } from "c/mlm_lwc_pubsub";      
 
 export default class new_widget_lwcParent extends NavigationMixin(
   LightningElement
 ) {
   @track disableButtonTotal = true;
   @track disableButtonPositive = true;
   @track disableButton;
   @track mode = "edit";
   @api type;
   @api index;
   @track disabledTest = true;
   @track tcaSpinner = false;
   @api recordId;
   @track disableButtonTotal = false;
   @track disabledButtonPositive = false;
   @track showsmartList = false;
   @track precompliance = false;
   @track homologation = false;
   @track isdeletedpushtopic = false;
   @track lessthan = 0;
   
   @track showtcaTest = false; 
   @track showtcaValidation = false;
   @wire(CurrentPageReference) pageRef;
 
   keyIndex = 0;
   @track itemList = [];
 

 
   doInit() {
    this.tcaSpinner = true;
    getAllWidgetTest({ aRecordId: this.recordId})
      .then((result) => { 
        console.log('recordId' + this.recordId);  
        if (result.length > 0) { 
          this.itemList = result;
          this.disabledTest = false;         
        } 

        console.log('doInit');  

        this.tcaSpinner = false;
        
      })
      .catch((error) => {
        this.tcaSpinner = false;
        this.error = error;
      });
  }
 
   itemPush() {
     let component = this;
     let testSession = 0;
     if (this.itemList.length == 0) {
       testSession = 1;
     } else { 
       this.itemList.forEach((res) => {
         if (res.Module_Tracke15__WidgetSession__c > testSession) {
             testSession = res.Module_Tracke15__WidgetSession__c;
         }
         testSession += 1;
       });
     }
     this.itemList.push({
       Id: null,
       Module_Tracke15__WidgetSession__c: testSession,
       Module_Tracke15__Value__c: "",
       Module_Tracke15__Is_Properly_Nested__c: false
     });  
   }
 
   connectedCallback() {
    this.showsmartList = true;  
     this.doInit();
   }

 
   deleteSession(event) {
     let rowIndex = event.detail.index;
     this.itemList.splice(rowIndex, 1);
   }
 
   get itemListLength() {
     return this.itemList.length;
   }

 
   disabledSession() {
     this.disabledTest = false;
   }
 
   enabledSession() {
     this.disabledTest = true;
   }
 
   canSave(confirmedButton) {
     if (this.itemList.length == 0) {
       confirmedButton.disabled = false;
     } else if (confirmedButton) {
       confirmedButton.disabled = this.disabledTest;
     } else if (this.itemList.length > 0) {
       this.enabledSession();
     }
   } 
 
   renderedCallback() {
     let confirmedButton = this.template.querySelector(
       '[data-id="sessiontest"]'
     );
     if (confirmedButton) {
       this.canSave(confirmedButton);
     }
   }
 
   addRow() {
     this.lessthan = -1;
     this.tcaSpinner = true;
     this.itemPush();
     let component = this;
 
     this.isdeletedpushtopic = false;
     this.tcaSpinner = false;
     this.disabledTest = true;
   } 
 
  
 }