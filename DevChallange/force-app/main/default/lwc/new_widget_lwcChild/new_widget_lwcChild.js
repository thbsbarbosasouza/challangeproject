import { LightningElement, api, track, wire } from 'lwc';
import getCreatedWidgets from '@salesforce/apex/WidgetController.getCreatedWidgets';
import getDeleteWidget from '@salesforce/apex/WidgetController.getDeleteWidget'; 
import is_nested_correctly from '@salesforce/label/c.isnestedcorrectly';
import customWidget from '@salesforce/label/c.CustomWidget';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';  
import { CurrentPageReference } from 'lightning/navigation';    
import { registerListener, unregisterAllListeners, fireEvent } from 'c/mlm_lwc_pubsub'; 
const EDIT = 'edit';
const TEST = 'Test';  

export default class new_widget_lwcChild extends LightningElement { 


    @api item;   
    @track aRecordId;  
    @track disableButtonTotal = true;
    @track disableButtonPositive = true;
    @api horizontalAlign = 'end'; 
    @track horizontalleft = 'left';
    @track verticalAlign = 'end';
    @track centerAlign = 'center';
    @track mode = 'read';
    @track currentrecord; 
    @track tcaWidget;  
    @api index; 
    @api childtype;  
    @api lessthanchild;  
    @track tcaSpinner = false;    
    @api itemlength;
    @api techconfid; 
    @track edit = 'edit';
    @track isconferma = false;
    @api childcurrentphase;  
    @api showname = false;
    @api recordId;


    label = {
        is_nested_correctly,
        customWidget 
    };


    get tcaItemId() {
        return this.tcaWidget.Id; 
    } 


    get title() {     
        console.log('this.tcaWidget.Module_Tracke15__WidgetSession__c' + this.tcaWidget.Module_Tracke15__WidgetSession__c); 
        return this.label.customWidget.replace('{0}', this.tcaWidget.Module_Tracke15__WidgetSession__c);
    }


    deleteSession() {
        let delSession = new CustomEvent('deletetca', { detail: { index: this.index } });
        this.dispatchEvent(delSession);
    }



    disabledSession() {
        let disabledSession = new CustomEvent('disabledtca');
        this.dispatchEvent(disabledSession);
    } 

    enabledSession(){
        let enabledSession = new CustomEvent('enabledtca');
        this.dispatchEvent(enabledSession); 
    }



    connectedCallback() {  
        this.tcaSpinner = true; 
        console.log('entrou no test');   
          
        this.tcaSpinner = false;     

            this.tcaWidget = JSON.parse(JSON.stringify(this.item));    
            if (!this.tcaWidget) { 
                this.editTCA();
            } 
            if (this.tcaWidget) {   
                this.isconferma = false;
            }
         
    } 


    containsValue(event) {      
        this.tcaWidget.Module_Tracke15__Value__c = event.target.value; 
    } 

    isProperlyNested(event) {   
        this.tcaWidget.Module_Tracke15__Is_Properly_Nested__c = event.target.checked;
    }    
 
 
    editTCA(event) {    
        this.isconferma = true;
        this.tcaSpinner = true;
        this.disableButtonTotal = false;
        this.disableButtonPositive = true; 
        this.mode = EDIT;
        this.tcaWidget.Module_Tracke15__Value__c = '';
        this.tcaWidget.Module_Tracke15__Is_Properly_Nested__c = ''; 
        this.tcaSpinner = false;  
        this.enabledSession();   
    }


    get disabledDelete() {
        if (this.index != this.itemlength - 1) {
            return true;
        } else {
            return false;
        }
    }

    get disabledConfirm() {
        if (this.tcaWidget.Module_Tracke15__Value__c &&
            this.mode == EDIT) {  
            return false;
        }
        else {
            return true;
        }
    } 


    renderedCallback() { 
        let confirmedButton = this.template.querySelector('[data-id="confirmed"]');
        if (confirmedButton) {
            confirmedButton.disabled = this.disabledConfirm;
        }
        let confirmedDelete = this.template.querySelector('[data-id="deleteidtca"]');
        if (confirmedDelete) {
            confirmedDelete.disabled = this.disabledDelete;
        }
    } 



    handleSubmit_Widget(event) {
        this.tcaSpinner = true; 
        console.log('recordId' + this.recordId);  
        getCreatedWidgets({ widgetRecord: this.tcaWidget, aRecordId : this.recordId, value : this.tcaWidget.Module_Tracke15__Value__c})
            .then(result => {     
  
                if(result.Module_Tracke15__Is_Properly_Nested__c == false){
                    this.showToastMessage('Warning', 'The text is not nested correctly', 'warning');
                    return;  
                } 
                else if(result.Module_Tracke15__Is_Properly_Nested__c == true){ 
                    this.message = this.label.is_nested_correctly.replace('{0}', result.Name);
                    console.log('if true');   
                    this.showToastMessage('Success', this.message, 'success'); 
                    this.tcaWidget.Module_Tracke15__Is_Properly_Nested__c = result.Module_Tracke15__Is_Properly_Nested__c; 
                    this.disabledSession();  
                } 
                this.tcaWidget.Id = result.Id;  
                eval("$A.get('e.force:refreshView').fire();");
                this.tcaSpinner = false; 
                this.tcaSpinner = false;
                this.isconferma = false;    
                this.disableButtonTotal = true;
                this.disableButtonPositive = true;
 

            }).catch(error => {
                console.log(error); 
                this.showToastMessage('Error', error.body.message, 'error');
                this.tcaSpinner = false;
            });

    }


    deleteTCA(event) {
        this.tcaSpinner = true;
        if (!this.tcaItemId) {  
            this.deleteSession();
            this.disabledSession();  
            return;    
        } 
        getDeleteWidget({ aRecordId: this.tcaWidget.Id }) 
            .then(() => {  
                this.deleteSession();
                this.showToastMessage('Success', 'Delete with success', 'success');
                this.tcaSpinner = false; 
                this.disabledSession();
                eval("$A.get('e.force:refreshView').fire();");
                this.deleteSession();  
                this.tcaSpinner = false;   
                window.location.reload(); 
                                
            })
            .catch(error => {
                this.tcaSpinner = false;
                console.log(error);
            });
    } 


    get disabledDelete() {
        if (this.index != this.itemlength - 1) {
            return true;
        } else {
            return false;
        }
    }


    get disabledConfirm() {   
        console.log('this.tcaWidget.Module_Tracke15__Value__c' + this.tcaWidget.Module_Tracke15__Value__c);
        console.log('this.tcaWidget.Module_Tracke15__Is_Properly_Nested__c' + this.tcaWidget.Module_Tracke15__Is_Properly_Nested__c); 
        if (this.tcaWidget.Module_Tracke15__Value__c && 
            this.mode == EDIT) { 
                
            return false;
        }
        else { 
        
            return true;
        }
    }


    showToastMessage(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title,
                message,
                variant
            })
        );
    }
}